***** Installing the Server and Repo Tools *****

sudo apt update
sudo apt install fdroidserver


***** Setup an F-Droid App Repo *****

https://opensource.com/article/18/3/configuring-multiple-web-sites-apache


https://f-droid.org/docs/Setup_an_F-Droid_App_Repo/
https://f-droid.org/docs/Signing_Process/


***** GitLab Pages *****

https://docs.gitlab.com/ee/user/project/pages/
https://www.youtube.com/watch?v=Cs6YxW9mr6Y
https://gitlab.com/gitlab-org/gitlab-foss/-/tree/master/lib/gitlab/ci/templates/Pages

https://gitlab.com/pages/plain-html
https://pages.gitlab.io/plain-html

+ Set up CI/CD
Browse templates - HTML.gitlab-ci.yml

git clone https://gitlab.com/DanKalinin.GitLab.Pages/Main.git
cd Main
mkdir -p public/fdroid
cd public/fdroid
fdroid init

cp ~/Downloads/1.apk repo
fdroid update --create-metadata --rename-apks

sudo apt install git-lfs
git lfs install
git lfs track "*.apk"

git add -A
git commit -m "fdroid"
git push origin main

https://gitlab.com/DanKalinin.GitLab.Pages/Main.git
Deploy -> Pages
https://main-dankalinin-gitlab-pages-ab0163631732bce931bc3be4a349c7e5e6.gitlab.io/


* fdroid init

INFO: Generating a new key in "keystore.jks"...
INFO: Alias name: dan-MacBookPro
Creation date: Mar 6, 2024
Entry type: PrivateKeyEntry
Certificate chain length: 1
Certificate[1]:
Owner: CN=dan-MacBookPro, OU=F-Droid
Issuer: CN=dan-MacBookPro, OU=F-Droid
Serial number: 79810d7
Valid from: Wed Mar 06 00:17:30 MSK 2024 until: Sun Jul 23 00:17:30 MSK 2051
Certificate fingerprints:
	 SHA1: D9:F4:9B:AE:00:71:D3:FF:C4:EA:29:31:DB:6F:87:2F:10:9D:54:FE
	 SHA256: 9B:F5:1E:31:ED:5C:49:D6:94:5B:73:47:40:85:D4:70:BF:2E:DB:B6:54:C0:92:B2:D1:46:B2:BB:13:91:01:DF
Signature algorithm name: SHA256withRSA
Subject Public Key Algorithm: 4096-bit RSA key
Version: 3

Extensions: 

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 05 E1 45 2A 2C 90 B9 D3   13 A6 81 2A 29 0E FE 64  ..E*,......*)..d
0010: 87 F2 82 10                                        ....
]
]


INFO: 
Built repo based in "/home/dan/Main/public/fdroid" with this config:

  Android SDK:			$ANDROID_HOME
  Android NDK r12b (optional):	$ANDROID_NDK
  Keystore for signing key:	keystore.jks

To complete the setup, add your APKs to "/home/dan/Main/public/fdroid/repo"
then run "fdroid update -c; fdroid update".  You might also want to edit
"config.py" to set the URL, repo name, and more.  You should also set up
a signing key (a temporary one might have been automatically generated).

For more info: https://f-droid.org/docs/Setup_an_F-Droid_App_Repo
and https://f-droid.org/docs/Signing_Process


* fdroid update --create-metadata --rename-apks

WARNING: Requested API level 33 is larger than maximum we have, returning API level 28 instead.
WARNING: Requested API level 33 is larger than maximum we have, returning API level 28 instead.
INFO: Using APK Signature v2
WARNING: Requested API level 33 is larger than maximum we have, returning API level 28 instead.
INFO: Using APK Signature v2
WARNING: repo/1.apk is set to android:debuggable="true"
INFO: Generated skeleton metadata for dan.electronics.kiosk
INFO: Creating signed index with this key (SHA256):
INFO: 9B F5 1E 31 ED 5C 49 D6 94 5B 73 47 40 85 D4 70 BF 2E DB B6 54 C0 92 B2 D1 46 B2 BB 13 91 01 DF
INFO: Creating signed index with this key (SHA256):
INFO: 9B F5 1E 31 ED 5C 49 D6 94 5B 73 47 40 85 D4 70 BF 2E DB B6 54 C0 92 B2 D1 46 B2 BB 13 91 01 DF
INFO: Finished


* fdroid init --help

usage: fdroid init [-h] [-v] [-q] [-d DISTINGUISHED_NAME] [--keystore KEYSTORE] [--repo-keyalias REPO_KEYALIAS] [--android-home ANDROID_HOME] [--no-prompt]

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         Spew out even more information than normal
  -q, --quiet           Restrict output to warnings and errors
  -d DISTINGUISHED_NAME, --distinguished-name DISTINGUISHED_NAME
                        X.509 'Distinguished Name' used when generating keys
  --keystore KEYSTORE   Path to the keystore for the repo signing key
  --repo-keyalias REPO_KEYALIAS
                        Alias of the repo signing key in the keystore
  --android-home ANDROID_HOME
                        Path to the Android SDK (sometimes set in ANDROID_HOME)
  --no-prompt           Do not prompt for Android SDK path, just fail


* fdroid update --help

usage: fdroid update [-h] [-v] [-q] [--create-key] [-c] [--delete-unknown]
                     [-b] [-I] [-w] [--pretty] [--clean] [--nosign]
                     [--use-date-from-apk] [--rename-apks]
                     [--allow-disabled-algorithms] [-W {error,warn,ignore}]

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         Spew out even more information than normal
  -q, --quiet           Restrict output to warnings and errors
  --create-key          Add a repo signing key to an unsigned repo
  -c, --create-metadata
                        Add skeleton metadata files for APKs that are missing
                        them
  --delete-unknown      Delete APKs and/or OBBs without metadata from the repo
  -b, --buildreport     Report on build data status
  -I, --icons           Resize all the icons exceeding the max pixel size and
                        exit
  -w, --wiki            Update the wiki
  --pretty              Produce human-readable XML/JSON for index files
  --clean               Clean update - don't uses caches, reprocess all APKs
  --nosign              When configured for signed indexes, create only
                        unsigned indexes at this stage
  --use-date-from-apk   Use date from APK instead of current time for newly
                        added APKs
  --rename-apks         Rename APK files that do not match
                        package.name_123.apk
  --allow-disabled-algorithms
                        Include APKs that are signed with disabled algorithms
                        like MD5
  -W {error,warn,ignore}
                        force metadata errors (default) to be warnings, or to
                        be ignored.

*

/home/dan/Downloads/fdroidserver-2.2.1/fdroid init
/home/dan/Downloads/fdroidserver-2.2.1/fdroid update --create-metadata --pretty --rename-apks

* config.yml
repo_url
repo_name

* metadata/*.yml
AuthorName
Description
Name
