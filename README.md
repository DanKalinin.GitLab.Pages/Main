## public/*/fdroid

```sh
fdroid init
fdroid update --create-metadata --pretty --rename-apks
```

## repo/*.apk

## config.yml

```yml
repo_url: https://main-dankalinin-gitlab-pages-681cf87dff66e2f5294514f7b81b754af3.gitlab.io/*/fdroid/repo
repo_name: Dan electronics
```

## metadata/*.yml

```yml
AuthorName: 'Dan electronics'
Name: Intercom
Description: "Bug fixes\nUI improvements\nAdded new styles"
```
